# interpolate

Одномерная интерполяция — это область построения кривой, которая бы полностью соответствовала набору двумерных точек данных. В SciPy есть функция interp1d, которая используется для создания одномерной интерполяции.

![](https://gitlab.com/Antoniii/interpolate/-/raw/main/index.png)

## Source

* [Руководство по SciPy](https://pythonru.com/biblioteki/scipy-python)
