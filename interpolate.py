#!/usr/bin/env python
# coding: utf-8

import matplotlib.pyplot as plt
from scipy import interpolate
import numpy as np

get_ipython().run_line_magic('matplotlib', 'inline')

x = np.arange(5, 20)
y = np.exp(x/3.0)
f = interpolate.interp1d(x, y)
x1 = np.arange(6, 18)
y1 = f(x1)   # использовать функцию интерполяции, возвращаемую `interp1d`
plt.plot(x, y, 'o', x1, y1, '--')

#plt.show()